﻿var arr = [];
var xMax = 6;
var yMax = 6;
var bombValue = "9";
var bombCount = 8;
var gameInProgress = false;

function bombGenerate( btn ) {
    var bombsGenerated = 0;
    do {
        var xRnd = Math.floor(Math.random() * 6);
        var yRnd = Math.floor(Math.random() * 6);

        var arrElem = arr[xRnd][yRnd];

        if( arrElem.id != btn.id && arrElem.value != bombValue ) {
            arrElem.value = bombValue;
            bombsGenerated++;
        }
    } while( bombsGenerated < bombCount );
};

function bombsNearby( x, y ) {
    var bombsCounter = 0;
    if ( x - 1 >= 0 ) {
        if ( y - 1 >= 0 ) {
            if ( arr[x-1][y-1].value === bombValue )
                bombsCounter++;
        }
        if ( y + 1 < yMax ) {
            if (arr[x - 1][y + 1].value === bombValue)
                bombsCounter++;
        }
        if (arr[x - 1][y].value === bombValue ) {
            bombsCounter++;
        }
    }

    if ( x + 1 < xMax ) {
        if ( y - 1 >= 0 ) {
            if ( arr[x+1][y-1].value === bombValue )
                bombsCounter++;
        }
        if ( y + 1 < yMax ) {
            if ( arr[x+1][y+1].value === bombValue )
                bombsCounter++;
        }
        if( arr[x+1][y].value === bombValue )
            bombsCounter++;
    }

    if ( y - 1 >= 0 ) {
        if ( arr[x][y-1].value === bombValue )
            bombsCounter++;
    }
    if ( y + 1 < yMax ) {
        if ( arr[x][y+1].value === bombValue )
            bombsCounter++;
    }

    return bombsCounter.toString();
};

function bombsOnBoard() {
    for (var x = 0; x < xMax; x++) {
        for( var y = 0; y < yMax; y++) {
            if(arr[x][y].value != bombValue)
                arr[x][y].value = bombsNearby( x, y );
        }
    }
};

function uncoverRecursively( x, y ) {
    if( arr[x][y].disabled ) {
        return;
    }

    if( arr[x][y].value != "0" ) {
        arr[x][y].disabled = 'disabled';
        arr[x][y].style.color = "black";
        arr[x][y].style.backgroundColor = "lightgray";
        return;
    }

    arr[x][y].disabled = 'disabled';
    arr[x][y].style.backgroundColor = "lightgray";


    if ( x - 1 >= 0 ) {
        if ( y - 1 >= 0 ) {
            uncoverRecursively( x - 1, y - 1);
        }
        if ( y + 1 < yMax ) {
            uncoverRecursively( x - 1, y + 1);
        }            
        uncoverRecursively( x - 1, y );
    }

    if ( x + 1 < xMax ) {
        if ( y - 1 >= 0 ) {
            uncoverRecursively( x + 1, y - 1);
        }
        if ( y + 1 < yMax ) {
            uncoverRecursively( x + 1, y + 1);
        }
        uncoverRecursively( x + 1, y );
    }

    if ( y - 1 >= 0 ) {
        uncoverRecursively( x , y - 1);
    }
    if ( y + 1 < yMax ) {
        uncoverRecursively( x, y + 1);
    }
}

function winCheck() {
    for( var x = 0; x < xMax; x++ ) {
        for( var y = 0; y < yMax; y++ ) {
            var btn = arr[x][y];
            if( !btn.disabled && btn.value != bombValue ) {
                return false;
            }
        }
    }
    return true;
}

function bombImg() {
    for (var x = 0; x < xMax; x++) {
        for (var y = 0; y < yMax; y++) {
            if( arr[x][y].value === bombValue ) {
                arr[x][y].style.background = "url(saper.jpg) center no-repeat";
            }
        }
    } 
}

function btnOnClick( btn ) {
    if(btn.value === "-") {
        bombGenerate( btn );
        bombsOnBoard();
    }

    if (btn.value === bombValue) {
        alert("You lose :(");
        document.body.style.background = "url(saper_bg.gif) no-repeat";
        document.body.style.backgroundSize = "100%";
//        document.body.style.backgroundImage = "100%";
        bombImg();
        for (var x = 0; x < xMax; x++) {
            for (var y = 0; y < yMax; y++) {
                if( arr[x][y].value != "0" && arr[x][y].value != bombValue){
                    arr[x][y].style.color = "black";
                }
                arr[x][y].disabled = 'disabled';
                arr[x][y].style.backgroundColor = "lightgray";
            }
        }
        gameInProgress = true;
        return;
    }
    else if ( btn.value === "0" ) {
        var x = parseInt( btn.id.substring( 3, 4 ) );
        var y = parseInt( btn.id.substring( 4, 5 ) );
        uncoverRecursively( x, y );
    }
    else {
        btn.disabled = 'disabled';
        btn.style.color = "black";
        btn.style.backgroundColor = "lightgray";
        gameInProgress = false;
    }

    if ( winCheck() ) {
        alert("WYGRANA!");
        bombImg();
        for (var x = 0; x < xMax; x++) {
            for (var y = 0; y < yMax; y++) { 
                    arr[x][y].disabled = 'disabled';
                    btn.style.backgroundColor = "lightgray";
            }
        }
        gameInProgress = true;   
    }
};

function reload() {
    if( !gameInProgress ) {
        if (confirm('Are you sure you want to play new game?') ) {
            location.reload(true);
        }
    }
    else {
        location.reload(true); 
    }
}

function bodyOnLoad() {
    gameInProgress = true;
    var str = "btn";
    for (var x = 0; x < xMax; x++) {
        var mid = [];
        for (var y = 0; y < yMax; y++) {
            var nr = (x).toString() + (y).toString();
            mid[y] = document.getElementById(str + nr);
            mid[y].disabled = false;
        }
        arr[x] = mid;
    }
};
